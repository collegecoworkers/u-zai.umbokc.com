{!! Form::open(['url' => isset($model) ? '/entity/update/'.$model->id : '/entity/create', 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<label class="col-sm-2 control-label">Название</label>
	<div class="col-sm-10">
		{!! Form::text('title', isset($model) ? $model->title : '', ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Телефон</label>
	<div class="col-sm-10">
		{!! Form::text('phone', isset($model) ? $model->phone : '', ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Адрес</label>
	<div class="col-sm-10">
		{!! Form::text('address', isset($model) ? $model->address : '', ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Описание</label>
	<div class="col-sm-10">
		{!! Form::textarea('desc', isset($model) ? $model->desc : '', ['required' => '','class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		<button type="submit" class="md-raised btn-w-md md-primary md-button md-ink-ripple">
			{{isset($model) ? 'Изменить' : 'Добавить'}}
		</button><div class="divider"></div>
	</div>
</div>
{!! Form::close() !!}
