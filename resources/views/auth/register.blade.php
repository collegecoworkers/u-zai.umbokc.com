@extends('../layouts.auth')
@section('content')
<div class="page-signin" ng-controller="authCtrl">
	<div class="wrapper" style="margin-top: 0;">
		<div class="main-body">
			<div class="body-inner">
				<div class="card bg-white">
					<div class="card-content">
						<section class="logo text-center">
							<h1>{{ config('app.name', 'Laravel') }}<br>Регистрация</h1>
						</section>
							{!! Form::open(['url' => '/register', 'class' => 'form-horizontal', 'id'=>'submit-form']) !!}
							<fieldset>
								<div class="form-group">
									<div class="ui-input-group">
										{!! Form::text('name', '', ['required' => '','class' => 'form-control']) !!}
										<span class="input-bar"></span>
										<label>Логин</label>
									</div>
									@if ($errors->has('name'))<span class="help-block"><strong c#f>{{ $errors->first('name') }}</strong></span> @endif
								</div>
								<div class="form-group">
									<div class="ui-input-group">
										{!! Form::text('full_name', '', ['required' => '','class' => 'form-control']) !!}
										<span class="input-bar"></span>
										<label>Полное имя</label>
									</div>
									@if ($errors->has('full_name'))<span class="help-block"><strong c#f>{{ $errors->first('full_name') }}</strong></span> @endif
								</div>
								<div class="form-group">
									<div class="ui-input-group">
										{!! Form::text('email', '', ['required' => '','class' => 'form-control']) !!}
										<span class="input-bar"></span>
										<label>Email</label>
									</div>
									@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span> @endif
								</div>
								<div class="form-group">
									<div class="ui-input-group">
										{!! Form::password('password', ['required' => '','class' => 'form-control']) !!}
										<span class="input-bar"></span>
										<label>Пароль</label>
									</div>
									@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span> @endif
								</div>
								<div class="form-group">
									<div class="ui-input-group">
										{!! Form::password('password_confirmation', ['required' => '','class' => 'form-control']) !!}
										<span class="input-bar"></span>
										<label>Пароль еще раз</label>
									</div>
									@if ($errors->has('password_confirmation'))<span class="help-block"><strong c#f>{{ $errors->first('password_confirmation') }}</strong></span> @endif
								</div>
							</fieldset>
						{!! Form::close() !!}
					</div>
					<div class="card-action no-border text-right">
						<a href="/" onclick="document.getElementById('submit-form').submit(); return false;" class="color-primary">Зарегистрироваться</a>
					</div>
				</div>
				<div class="additional-info">
					<a href="{{ route('login') }}">Войти</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

