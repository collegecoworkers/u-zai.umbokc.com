@extends('../layouts.auth')
@section('content')
<div class="page-signin" ng-controller="authCtrl">
	<div class="wrapper">
		<div class="main-body">
			<div class="body-inner">
				<div class="card bg-white">
					<div class="card-content">
						<section class="logo text-center">
							<h1>{{ config('app.name', 'Laravel') }}<br>Вход</h1>
						</section>
							{!! Form::open(['url' => '/login', 'class' => 'form-horizontal', 'id'=>'submit-form']) !!}
							<fieldset>
								<div class="form-group">
									<div class="ui-input-group">        
										{!! Form::text('email', '', ['required' => '','class' => 'form-control']) !!}
										<span class="input-bar"></span>
										<label>Email</label>
									</div>
									@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span> @endif
								</div>
								<div class="form-group">
									<div class="ui-input-group">
										{!! Form::password('password', ['required' => '','type' => 'password','class' => 'form-control']) !!}
										<span class="input-bar"></span>
										<label>Пароль</label>
									</div>
									@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span> @endif
								</div>
							</fieldset>
						{!! Form::close() !!}
					</div>
					<div class="card-action no-border text-right">
						<a href="/" onclick="document.getElementById('submit-form').submit(); return false;" class="color-primary">Войти</a>
					</div>
				</div>
				<div class="additional-info">
					<a href="{{ route('register') }}">Зарегистрироваться</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
