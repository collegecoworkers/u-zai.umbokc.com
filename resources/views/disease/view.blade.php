@php
use App\{
	User,
	Doktor,
	Entity,
	Disease,
	Registry,
	Specification
};
@endphp
@extends('../layouts.app')
@section('content')
<div class="page page-profile">
	<div class="divider divider-lg"></div>
	<div class="row ui-section">
		<div class="col-lg-8 clearfix">
			<h2 class="section-header">История болезней: {{$user->full_name}}</h2>
		</div>
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<table class="table no-margin">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Описание</th>
								<th>Доктор</th>
								<th>Дата</th>
								<th>Действия</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($diseases as $item)
							<tr>
								<td>{{$item->id}}</td>
								<td>{{$item->title}}</td>
								<td>{{$item->desc}}</td>
								<td>{{User::getBy('id', $item->doktor_id)->full_name}}</td>
								<td>{{ $item->getDateCreate() }}</td>
								<td td:n@childs cur:p@childs>
									<a href="{{ route('/disease/edit/{id}', ['id'=>$item->id]) }}">
										<i class="material-icons">edit</i>
									</a>
									<a href="{{ route('/disease/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
										<i class="material-icons">delete</i>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
