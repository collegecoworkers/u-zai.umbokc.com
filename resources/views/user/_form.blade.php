{!! Form::open(['url' => isset($model) ? '/user/update/'.$model->id : '/user/create', 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<label class="col-sm-2 control-label">Логин</label>
	<div class="col-sm-10">
		{!! Form::text('name', isset($model) ? $model->name : '', ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Имя</label>
	<div class="col-sm-10">
		{!! Form::text('full_name', isset($model) ? $model->full_name : '', ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Email</label>
	<div class="col-sm-10">
		{!! Form::text('email', isset($model) ? $model->email : '', ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Роль</label>
	<div class="col-sm-10">
		{!! Form::select('role', $roles, isset($model) ? $model->role : '', ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Пароль</label>
	<div class="col-sm-10">
		{!! Form::password('password', ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		<button type="submit" class="md-raised btn-w-md md-primary md-button md-ink-ripple">
			{{isset($model) ? 'Изменить' : 'Добавить'}}
		</button><div class="divider"></div>
	</div>
</div>
{!! Form::close() !!}
