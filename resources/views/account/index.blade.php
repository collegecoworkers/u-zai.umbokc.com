@php
use App\{
	User,
	Doktor,
	Entity,
	Disease,
	Registry,
	Specification
};
@endphp
@extends('../layouts.app')
@section('content')
<div class="page page-dashboard" data-ng-controller="DashboardCtrl">

	<div class="row">
		<div class="col-lg-4 col-xsm-6">
			<div class="panel panel-box">
				<div class="panel-top">
					<i class="material-icons color-success">pregnant_woman</i>
				</div>
				<div class="panel-info">
					<span>Доктора</span>
				</div>
				<div class="panel-bottom bg-light">
					<span>{{ Doktor::count() }}</span>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-xsm-6">
			<div class="panel panel-box">
				<div class="panel-top">
					<i class="material-icons color-success">supervisor_account</i>
				</div>
				<div class="panel-info">
					<span>Пациенты</span>
				</div>
				<div class="panel-bottom bg-light">
					<span>{{ User::getsBy('role', 'user')->count() }}</span>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-xsm-6">
			<div class="panel panel-box">
				<div class="panel-top">
					<i class="material-icons color-success">history</i>
				</div>
				<div class="panel-info">
					<span>Записи на прием</span>
				</div>
				<div class="panel-bottom bg-light">
					<span>{{ Registry::count() }}</span>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection
