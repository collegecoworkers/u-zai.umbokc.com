@php
use App\{
	User,
	Doktor,
	Entity,
	Registry,
	Specification
};
@endphp
@extends('../layouts.app')
@section('content')
<div class="page page-profile">
	<div class="divider divider-lg"></div>
	<div class="row ui-section">
		<div class="col-lg-8 clearfix">
			<h2 class="section-header">Заявки</h2>
		</div>
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<table class="table no-margin">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Дата</th>
								<th>Пользователь</th>
								@if (!User::isDok())
									<th>Доктор</th>
									<th>Спецификация</th>
									<th>Учреждение</th>
								@endif
								<th>Статус</th>
								@if (User::isDok())
									<th>Описание</th>
								@endif
								@if (!User::isUser())
									<th>Действия</th>
								@endif
							</tr>
						</thead>
						<tbody>
							@foreach ($registries as $item)
								<tr>
									<td>{{$item->id}}</td>
									<td>{{$item->title}}</td>
									<td>{{$item->getDate()}}</td>
									<td>{{ User::getBy('id', $item->user_id)->full_name }}</td>
									@if (!User::isDok())
										@php
											$dok = Doktor::getBy('id', $item->doktor_id);
										@endphp
										<td>{{ User::getBy('id', $dok->user_id)->full_name }}</td>
										<td>{{ Specification::getBy('id', $dok->specific_id)->title }}</td>
										<td>{{ Entity::getBy('id', $dok->entity_id)->title }}</td>
									@endif
									<td>{{$item->getStatus()}}</td>
									@if (User::isDok())
										<td>{{$item->desc}}</td>
									@endif
									@if (!User::isUser())
									<td td:n@childs cur:p@childs>
										<a href="{{ route('/registry/edit/{id}', ['id'=>$item->id]) }}">
											<i class="material-icons">edit</i>
										</a>
										<a href="{{ route('/registry/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
											<i class="material-icons">delete</i>
										</a>
									</td>
									@endif
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
