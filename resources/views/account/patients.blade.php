@php
use App\{
	User,
	Doktor,
	Entity,
	Disease,
	Registry,
	Specification
};
@endphp
@extends('../layouts.app')
@section('content')
<div class="page page-profile">
	<div class="divider divider-lg"></div>
	<div class="row ui-section">
		<div class="col-lg-8 clearfix">
			<h2 class="section-header">Пациенты</h2>
		</div>
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<table class="table no-margin">
						<thead>
							<tr>
								<th>#</th>
								<th>Имя</th>
								<th>Кол-во обращений</th>
								<th>Кол-во болезней</th>
								<th>Действия</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($patients as $item)
								<tr>
									<td>{{$item->id}}</td>
									<td>{{ $item->full_name }}</td>
									<td>{{ Registry::where('user_id', $item->id)->count() }}</td>
									<td>{{ Disease::where('user_id', $item->id)->count() }}</td>
									<td td:n@childs cur:p@childs>
										<a href="{{ route('/disease/add/{id}', ['id'=>$item->id]) }}">
											<i class="material-icons">add</i>
										</a>
										<a href="{{ route('/disease/view/{id}', ['id'=>$item->id]) }}">
											<i class="material-icons">visibility</i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
