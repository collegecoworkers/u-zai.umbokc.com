@php
use App\{
	User,
	Doktor,
	Entity,
	Registry,
	Specification
};
@endphp
@extends('../layouts.app')
@section('content')
<div class="page page-profile">
	<div class="divider divider-lg"></div>
	<div class="row ui-section">
		<div class="col-lg-8 clearfix">
			<h2 class="section-header">Доктора</h2>
		</div>
		<div class="col-lg-8 clearfix" m:b>
			<a href="{{ route('/doktor/add') }}" class="md-raised btn-w-md md-primary md-button md-ink-ripple" style="line-height: 3;">Добавить</a>
		</div>
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<table class="table no-margin">
						<thead>
							<tr>
								<th>#</th>
								<th>Имя</th>
								<th>Специальность</th>
								<th>Учреждение</th>
								<th>Действия</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($doktors as $item)
								<tr>
									<td>{{$item->id}}</td>
									<td>{{ User::getBy('id', $item->user_id)->full_name }}</td>
									<td>{{ Specification::getBy('id', $item->specific_id)->title }}</td>
									<td>{{ Entity::getBy('id', $item->entity_id)->title }}</td>
									<td td:n@childs cur:p@childs>
										<a href="{{ route('/doktor/edit/{id}', ['id'=>$item->id]) }}">
											<i class="material-icons">edit</i>
										</a>
										<a href="{{ route('/doktor/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
											<i class="material-icons">delete</i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
