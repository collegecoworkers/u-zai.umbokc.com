@php
Form::macro('mydatetime', function($name, $def_val = '', $args = []) {
	$years = value(function() {
		$startYear = (int) date('Y');
		$endYear = $startYear - 5;
		$years = ['' => 'year'];
		for($year = $startYear; $year > $endYear; $year--) {
			$years[ $year ] = $year;
		};
		return $years;
	});

	$months = value(function() {
		$months = ['' => 'month'];
		for($month = 1; $month < 13; $month++) {
			$timestamp = strtotime(date('Y'). '-'.$month.'-13');
			$months[ $month ] = strftime('%B', $timestamp);
		}
		return $months;
	});

	$days = value(function() {
		$days = ['' => 'day'];
		for($day = 1; $day < 32; $day++) {
			$days[ $day ] = $day;
		}
		return $days;
	});

	$hours = value(function() {
		$hours = ['' => 'hour'];
		for($hour = 0; $hour < 24; $hour++) {
			$hours[ $hour ] = $hour;
		}
		return $hours;
	});

	$minutes = value(function() {
		$minutes = ['' => 'minute'];
		for($minute = 0; $minute < 60; $minute++) {
			$minutes[ $minute ] = $minute;
		}
		return $minutes;
	});

	function getYesterday($f, $def_val){
		// "Y.n.j G:i "
		if(trim($def_val) != ''){
			return date($f, strtotime($def_val));
		} else{
			return date($f, time() + 86400);
		}
	}
	$args['style'] = 'width:auto;margin: 0 10px;';
	$args['fl:l'] = '';

	return Form::select($name.'[year]', $years, getYesterday('Y', $def_val), $args) .
	Form::select($name.'[month]', $months, getYesterday('n', $def_val), $args) .
	Form::select($name.'[day]', $days, getYesterday('j', $def_val), $args) . 
	' <span fl>-</span> ' .
	Form::select($name.'[hour]', $hours, getYesterday('G', $def_val), $args) .
	Form::select($name.'[minute]', $minutes, intval(getYesterday('i', $def_val)), $args);
});
@endphp
@extends('../layouts.main')
@section('content')
<style>
	.newsletter .ui-input-group>label.lb-s,
	.newsletter .ui-input-group>label[lb-s]{
		position: static;
		float: left;
	}
	[c\#f]{color: #FFF !important;}
</style>
<div id="newsletter" data-stellar-background-ratio="0.6" class="newsletter">
	<h2>Запишитесь на прием</h2>
	{!! Form::open(array('url' => '/registry/create', 'style'=>'min-width:600px')) !!}
		<fieldset>
			<div class="form-group">
				<div class="ui-input-group">
					{!! Form::text('title', '', ['required' => '','class' => 'form-control']) !!}
					<span class="input-bar"></span><label>Название</label>
				</div>
				<div class="ui-input-group">
					<label lb-s>Доктор</label>
					{!! Form::select('doktor_id', $doktors, '', ['required' => '', 'c#0' => '','class' => 'form-control']) !!}
				</div>
				<div class="ui-input-group">
					<label pos:s ta:l d:b>Дата</label>
					{!! Form::mydatetime('date', '', ['required' => '', 'c#0' => '','class' => 'form-control']) !!}
				</div>
				<div class="clearfix"></div>
				<div class="ui-input-group">
					{!! Form::textarea('desc', '', ['required' => '','rows' => '3','class' => 'form-control']) !!}
					<span class="input-bar"></span><label>Описание</label>
				</div>
			</div>
			<button type="submit" data-wow-delay="0.15s" class="btn btn-primary btn-lg ui-wave wow fadeInUp">Отправить</button>
		</fieldset>
	{!! Form::close() !!}
</div>
@endsection
