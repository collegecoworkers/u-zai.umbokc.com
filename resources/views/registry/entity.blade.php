@extends('../layouts.main')
@section('content')
<style>
.newsletter .ui-input-group>label.lb-s,
.newsletter .ui-input-group>label[lb-s]{
	position: static;
	float: left;
}
[c\#f]{color: #FFF !important;}
</style>
<div id="newsletter" data-stellar-background-ratio="0.6" class="newsletter">
	<h2>Запишитесь на прием</h2>
	<h3>Выберите учреждение</h3>
	{!! Form::open(array('url' => '/', 'style'=>'min-width:600px')) !!}
		<fieldset>
			<div class="form-group">
				<div class="ui-input-group">
					<label lb-s>Учреждение</label>
					{!! Form::select('entity', $entities, '', ['required' => '', 'c#0' => '','class' => 'form-control']) !!}
				</div>
			</div>
			<a data-wow-delay="0.15s" onclick="$(this).attr('href', location.href + '/ss/' + $('[name=entity]').val());return true;" class="btn btn-primary btn-lg ui-wave wow fadeInUp">Отправить</a>
		</fieldset>
		{!! Form::close() !!}
	</div>
	@endsection
