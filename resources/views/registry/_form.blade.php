@php
Form::macro('mydatetime', function($name, $def_val = '', $args = []) {
	$years = value(function() {
		$startYear = (int) date('Y');
		$endYear = $startYear - 5;
		$years = ['' => 'year'];
		for($year = $startYear; $year > $endYear; $year--) {
			$years[ $year ] = $year;
		};
		return $years;
	});

	$months = value(function() {
		$months = ['' => 'month'];
		for($month = 1; $month < 13; $month++) {
			$timestamp = strtotime(date('Y'). '-'.$month.'-13');
			$months[ $month ] = strftime('%B', $timestamp);
		}
		return $months;
	});

	$days = value(function() {
		$days = ['' => 'day'];
		for($day = 1; $day < 32; $day++) {
			$days[ $day ] = $day;
		}
		return $days;
	});

	$hours = value(function() {
		$hours = ['' => 'hour'];
		for($hour = 0; $hour < 24; $hour++) {
			$hours[ $hour ] = $hour;
		}
		return $hours;
	});

	$minutes = value(function() {
		$minutes = ['' => 'minute'];
		for($minute = 0; $minute < 60; $minute++) {
			$minutes[ $minute ] = $minute;
		}
		return $minutes;
	});

	function getYesterday($f, $def_val){
		// "Y.n.j G:i "
		if(trim($def_val) != ''){
			return date($f, strtotime($def_val));
		} else{
			return date($f, time() + 86400);
		}
	}
	$args['style'] = 'width:auto;margin: 0 10px;';
	$args['fl:l'] = '';

	return Form::select($name.'[year]', $years, getYesterday('Y', $def_val), $args) .
	Form::select($name.'[month]', $months, getYesterday('n', $def_val), $args) .
	Form::select($name.'[day]', $days, getYesterday('j', $def_val), $args) . 
	' <span fl>-</span> ' .
	Form::select($name.'[hour]', $hours, getYesterday('G', $def_val), $args) .
	Form::select($name.'[minute]', $minutes, intval(getYesterday('i', $def_val)), $args);
});
@endphp
{!! Form::open(['url' => '/registry/update/'.$model->id, 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<label class="col-sm-2 control-label">Название</label>
	<div class="col-sm-10">
		{!! Form::text('title', $model->title, ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Время</label>
	<div class="col-sm-10">
		{!! Form::mydatetime('date', $model->date, ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Доктор</label>
	<div class="col-sm-10">
		{!! Form::select('doktor_id', $doktors, $model->doktor_id, ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Статус</label>
	<div class="col-sm-10">
		{!! Form::select('status', $statuses, $model->status, ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Описание</label>
	<div class="col-sm-10">
		{!! Form::textarea('desc', $model->desc, ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		<button type="submit" class="md-raised btn-w-md md-primary md-button md-ink-ripple">
			Изменить
		</button><div class="divider"></div>
	</div>
</div>
{!! Form::close() !!}
