{!! Form::open(['url' => isset($model) ? '/doktor/update/'.$model->id : '/doktor/create', 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<label class="col-sm-2 control-label">Пользователь</label>
	<div class="col-sm-10">
		{!! Form::select('user_id', $user_ids, isset($model) ? $model->user_id : '', ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Учреждение</label>
	<div class="col-sm-10">
		{!! Form::select('entity_id', $entities, isset($model) ? $model->entity_id : '', ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Спецификация</label>
	<div class="col-sm-10">
		{!! Form::select('specific_id', $specifications, isset($model) ? $model->specific_id : '', ['required' => '','class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		<button type="submit" class="md-raised btn-w-md md-primary md-button md-ink-ripple">
			{{isset($model) ? 'Изменить' : 'Добавить'}}
		</button><div class="divider"></div>
	</div>
</div>
{!! Form::close() !!}
