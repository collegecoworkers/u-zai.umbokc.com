@extends('../layouts.app')
@section('content')

<div class="page">
	<div class="row ui-section">
		<div class="col-lg-8 clearfix">
			<h2 class="section-header">Новый доктор</h2>
		</div>
		<div class="col-md-12">
			<section class="panel panel-default">
				<div class="panel-body padding-xl">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							@include('doktor._form')
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection
