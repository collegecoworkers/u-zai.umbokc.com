<!DOCTYPE html>
<html class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link href="http://fonts.googleapis.com/css?family=Roboto:400,100,500,300,300italic,500italic|Roboto+Condensed:400,300" rel="stylesheet">
	<link rel="stylesheet" href="http://cdn.umbokc.com/ea/src/ea.css?v=1.3">
	<link href="{{ asset('assets/lg/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/lg/styles/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/lg/styles/main.css') }}" rel="stylesheet">

	<script src="{{ asset('assets/lg/scripts/modernizr.js') }}"></script>
</head>
<body data-spy="scroll" data-target=".header" data-offset="80" ea>
	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
	<div id="ip-container" class="ip-container">
		<header class="ip-header">
			<div class="ip-loader">
				<svg width="60px" height="60px" viewBox="0 0 80 80" class="ip-inner">
					<path d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z" class="ip-loader-circlebg"></path>
					<path id="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z" class="ip-loader-circle"></path>
				</svg>
			</div>
		</header>
		<div class="ip-main">
			<header id="header" class="header navbar navbar-light animated">
				<div class="container"><a href="#" class="navbar-brand"> <span class="logo">{{ mb_substr(config('app.name', 'Laravel'), 0, 1) }}</span><span class="brand-txt">{{ config('app.name', 'Laravel') }}</span></a>
					<div class="navbar">
						<ul class="nav navbar-nav pull-right">
							<li class="nav-item"><a href="{{ route('/registry') }}">Записаться</a></li>

							<li class="nav-item"><a href="{{ route('/account') }}">Личный кабинет</a></li>
							{{-- <li class="nav-item"><a href="{{ route('login') }}">Войти</a></li> --}}
							<li class="nav-item"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a></li>

						</ul>
						<div class="pull-right togger-container"><span id="showRight" class="zmdi zmdi-menu navbar-toggler"></span></div>
					</div>
				</div>
			</header>
			<div class="main-content">
				@yield('content')
			</div>
			<footer class="footer text-center"><span>© Copyright <span id="copyright"></span> {{ config('app.name', 'Laravel') }}</span></footer>
		</div>
	</div>
	<!-- Scripts-->
	<script src="{{ asset('assets/lg/scripts/app.js') }}"></script>
</body>
</html>
