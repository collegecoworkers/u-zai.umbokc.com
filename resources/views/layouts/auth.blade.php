<!doctype html>
<html class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv='X-UA-Compatible' content='IE=edge'>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">

	<!-- google fonts -->
	<script type="text/javascript">
		WebFontConfig = {
			google: { families: [ 'Roboto:100,300,400,400italic,500,700:latin' ] }
		};
		(function() {
			var wf = document.createElement('script');
			wf.src = '{{ asset('assets/dash/bower_components/webfontloader/webfontloader.js') }}';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})(); 
	</script>

	<!-- Needs images, font... therefore can not be part of main.css -->
	<link rel="stylesheet" href="{{ asset('assets/dash/styles/loader.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/dash/vendors/material-design-icons/iconfont/material-icons.css') }}" >
	<link rel="stylesheet" href="{{ asset('assets/dash/bower_components/font-awesome/css/font-awesome.min.css') }}">
	<!-- end Needs images -->
		
	<link rel="stylesheet" href="{{ asset('assets/dash/styles/main.css') }}">
</head>
<body data-ng-app="app" id="app" class="app" data-custom-page data-ng-controller="AppCtrl">

	<div id="loader-container"></div>

	<div class="main-container">
		<div id="content" class="content-container" style="margin-left: 0;">
			<section data-ui-view class="view-container @{{main.pageTransition.class}}">
				@yield('content')
			</section>
		</div>
	</div>

	<script src="{{ asset('assets/dash/scripts/vendor.js') }}"></script>
	<script src="{{ asset('assets/dash/scripts/ui.js') }}"></script>
	<script src="{{ asset('assets/dash/scripts/auth.js') }}"></script>
</body>
</html>
