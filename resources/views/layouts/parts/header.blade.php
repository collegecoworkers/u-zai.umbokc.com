<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
<header class="top-header clearfix ng-scope" m:l:big>
	<div ui-preloader></div>
	<div class="logo bg-success">
		<a href="/">
			<span class="logo-icon material-icons">dashboard</span>
			<span class="logo-text">{{ config('app.name', 'Laravel') }}</span>
		</a>
	</div>

	<div class="top-nav">
		<ul class="nav-right pull-right list-unstyled">
			<li>
				<md-menu md-position-mode="target-right target">
					<md-button class="header-btn" aria-label="menu" ng-click="$mdMenu.open($event)">
						{{ auth()->user()->name }}
					</md-button>
					<md-menu-content class="top-header-dropdown" width="3">
						<md-menu-item><md-button aria-label="menu" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><md-icon class="material-icons">keyboard_tab</md-icon><span>Выход</span></md-button></md-menu-item>
					</md-menu-content>
				</md-menu>
			</li>
		</ul>
	</div>
</header>
