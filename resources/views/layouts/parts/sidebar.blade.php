@php
	// use App\User;
	// $cur_role = User::curRole();
	$nav = [
		['visible' => true, 'url' => route('/account'), 'icon' => 'home', 'label' => 'Главная'],
		['visible' => $cur_role == 'user', 'url' => route('/account/history'), 'icon' => 'history', 'label' => 'История болезней'],
		['visible' => $cur_role == 'doktor' || $cur_role == 'admin', 'url' => route('/account/patients'), 'icon' => 'monetization_on', 'label' => 'Пациенты'],
		['visible' => true, 'url' => route('/account/registries'), 'icon' => 'history', 'label' => 'Записи на прием'],
		['visible' => $cur_role == 'admin', 'url' => route('/account/entities'), 'icon' => 'account_balance', 'label' => 'Учреждения'],
		['visible' => $cur_role == 'admin', 'url' => route('/account/specifications'), 'icon' => 'add_to_queue', 'label' => 'Спецификации'],
		['visible' => $cur_role == 'admin', 'url' => route('/account/doktors'), 'icon' => 'pregnant_woman', 'label' => 'Доктора'],
		['visible' => $cur_role == 'admin', 'url' => route('/account/users'), 'icon' => 'supervisor_account', 'label' => 'Пользователи'],
	];
@endphp
<div class="nav-wrapper">
	<ul id="nav" class="nav" data-slim-scroll data-collapse-nav data-highlight-active>
		@foreach ($nav as $item)
			@if ($item['visible'])
				<li {{ url()->current() == $item['url'] ? 'td:u' : ''}}>
					<a md-button aria-label="menu" href="{{ $item['url'] }}">
						<i class="material-icons">{{$item['icon']}}</i><span>{{$item['label']}}</span>
					</a>
				</li>
			@endif
		@endforeach
	</ul>
</div>
