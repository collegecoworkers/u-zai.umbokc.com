@extends('layouts.main')
@section('content')

<div id="hero" class="hero">
	<div class="hero-inner"> 
		<div class="container">
			<div class="col-lg-6 col-lg-offset-3">
				<h2>{{ config('app.name', 'Laravel') }}</h2>
				<p>Красивый слоган</p>
				<a href="{{ route('/registry') }}" class="btn btn-primary btn-cta btn-lg btn-raised ui-wave">Записаться на прием</a>
			</div>
		</div>
	</div>
</div>

@endsection
