<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class MyModel extends Model{
	protected static $allArr_field = 'title';
	public static function allArr(){
		return F::toArr(self::all(), static::$allArr_field, 'id');
	}
	public static function getBy($col, $val){
		return self::where($col, $val)->first();
	}
	public static function getsBy($col, $val){
		return self::where($col, $val)->get();
	}
}
