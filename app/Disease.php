<?php

namespace App;

class Disease extends MyModel
{
	protected $table = 'disease';
  protected static $allArr_field = 'title';
  public function getDateCreate() {
    return date('Y/m/d', strtotime($this->created_at));
  }
}
