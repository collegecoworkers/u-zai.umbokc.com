<?php

namespace App;

class Registry extends MyModel
{
	protected $table = 'registry';
	protected static $allArr_field = 'title';
	public function getDate() {
		return date('m/d H:i', strtotime($this->date));
	}

	public function getStatus() {
		return self::getStatusOf($this->status);
	}

	public static function getStatuss(){
		return [
			'0' => 'заявка подана',
			'1' => 'доктор подтвердил',
			'2' => 'заявка завершена',
			'3' => 'заявка отменена',
		];
	}

	public static function getStatusOf($r){
		$items = self::getStatuss();
		if(array_key_exists($r, $items)) 
			return $items[$r];
		return $items[0];
	}
}
