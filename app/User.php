<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;

	protected $fillable = [
		'name', 'email', 'password',
	];

	protected $hidden = [
		'password', 'remember_token'
	];

	public function getRole() {
		return self::getRoleOf($this->role);
	}

	public static function getRoles(){
		return [
			'user' => 'Пользователь',
			'admin' => 'Админ',
			'doktor' => 'Доктор',
		];
	}

	public static function getRoleOf($r){
		$roles = self::getRoles();
		if(array_key_exists($r, $roles)) 
			return $roles[$r];
		return $roles[0];
	}

	public static function isAdmin() {
		return self::curr()->role == 'admin';
	}

	public static function isUser() {
		return self::curr()->role == 'user';
	}

	public static function curr() {
		return auth()->user();
	}

	public static function isDok() {
		return self::curr()->role == 'doktor';
	}

	public static function curRole() {
		return self::curr()->role;
	}

	public static function getByDokId($id) {
		return self::getBy('id', Doktor::getBy('id', $id)->user_id);
	}

	public static function getBy($col, $val){
		return self::where($col, $val)->first();
	}
	public static function getsBy($col, $val){
		return self::where($col, $val)->get();
	}
	protected static $allArr_field = 'name';
	public static function allArr(){
		$items = self::all();
		$items_arr = [];
		foreach ($items as $item)
			$items_arr[$item->id] = $item->{static::$allArr_field};
		return $items_arr;
	}
}
