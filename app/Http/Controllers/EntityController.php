<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	User,
	Doktor,
	Entity,
	Registry,
	Specification
};

class EntityController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Add() {
		$this->the_before();
		return view('entity.add')->with([
			'cur_role' => $this->cur_role,
		]);
	}
	public function Edit($id) {
		$this->the_before();
		$model = Entity::getBy('id', $id);
		return view('entity.edit')->with([
			'cur_role' => $this->cur_role,
			'model' => $model,
		]);
	}
	public function Delete($id) {
		$this->the_before();
		Entity::where('id', $id)->delete();
		return redirect()->to('/account/entities');
	}
	public function Create(Request $request) {
		$this->the_before();
		$model = new Entity();

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->phone = request()->phone;
		$model->phone = request()->phone;
		$model->address = request()->address;

		$model->save();
		return redirect('/account/entities');
	}
	public function Update($id, Request $request) {
		$this->the_before();
		$model = Entity::getBy('id', $id);

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->phone = request()->phone;
		$model->phone = request()->phone;
		$model->address = request()->address;

		$model->save();
		return redirect()->to('/account/entities');
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
