<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	User,
	Doktor,
	Entity,
	Registry,
	Specification
};

class DoktorController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Add() {
		$this->the_before();
		return view('doktor.add')->with([
			'cur_role' => $this->cur_role,
			'user_ids' => F::toArr(User::getsBy('role', 'doktor'), 'name'),
			'entities' => Entity::allArr(),
			'specifications' => Specification::allArr(),
		]);
	}
	public function Edit($id) {
		$this->the_before();
		$model = Doktor::getBy('id', $id);
		return view('doktor.edit')->with([
			'cur_role' => $this->cur_role,
			'model' => $model,
			'user_ids' => User::allArr(),
			'entities' => Entity::allArr(),
			'specifications' => Specification::allArr(),
		]);
	}
	public function Delete($id) {
		$this->the_before();
		Doktor::where('id', $id)->delete();
		return redirect()->to('/account/doktors');
	}
	public function Create(Request $request) {
		$this->the_before();
		$model = new Doktor();

		$model->user_id = request()->user_id;
		$model->entity_id = request()->entity_id;
		$model->specific_id = request()->specific_id;

		$model->save();
		return redirect('/account/doktors');
	}
	public function Update($id, Request $request) {
		$this->the_before();
		$model = Doktor::getBy('id', $id);

		$model->user_id = request()->user_id;
		$model->entity_id = request()->entity_id;
		$model->specific_id = request()->specific_id;

		$model->save();
		return redirect()->to('/account/doktors');
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
