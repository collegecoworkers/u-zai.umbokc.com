<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	User,
	Doktor,
	Entity,
	Registry,
	Specification
};

class SpecificationController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Add() {
		$this->the_before();
		return view('specification.add')->with([
			'cur_role' => $this->cur_role,
		]);
	}
	public function Edit($id) {
		$this->the_before();
		$model = Specification::getBy('id', $id);
		return view('specification.edit')->with([
			'cur_role' => $this->cur_role,
			'model' => $model,
		]);
	}
	public function Delete($id) {
		$this->the_before();
		Specification::where('id', $id)->delete();
		return redirect()->to('/account/specifications');
	}
	public function Create(Request $request) {
		$this->the_before();
		$model = new Specification();

		$model->title = request()->title;
		$model->desc = request()->desc;

		$model->save();
		return redirect('/account/specifications');
	}
	public function Update($id, Request $request) {
		$this->the_before();
		$model = Specification::getBy('id', $id);

		$model->title = request()->title;
		$model->desc = request()->desc;

		$model->save();
		return redirect()->to('/account/specifications');
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
