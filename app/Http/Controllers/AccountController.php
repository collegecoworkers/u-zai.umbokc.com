<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	User,
	Doktor,
	Entity,
	Disease,
	Registry,
	Specification
};

class AccountController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {
		$this->the_before();
		return view('account.index')->with([
			'cur_role' => $this->cur_role,
		]);
	}
	public function History() {
		$this->the_before();
		return view('account.history')->with([
			'cur_role' => $this->cur_role,
			'diseases' => Disease::getsBy('user_id', auth()->user()->id),
		]);
	}
	public function Patients() {
		$this->the_before();
		$patients = User::getsBy('role', 'user');
		return view('account.patients')->with([
			'cur_role' => $this->cur_role,
			'patients' => $patients,
		]);
	}
	public function Entities() {
		$this->the_before();
		return view('account.entities')->with([
			'cur_role' => $this->cur_role,
			'entities' => Entity::all(),
		]);
	}
	public function Specifications() {
		$this->the_before();
		return view('account.specifications')->with([
			'cur_role' => $this->cur_role,
			'specifications' => Specification::all(),
		]);
	}
	public function Doktors() {
		$this->the_before();
		return view('account.doktors')->with([
			'cur_role' => $this->cur_role,
			'doktors' => Doktor::all(),
		]);
	}
	public function Registries() {
		$this->the_before();
		$registries = [];
		if (User::isUser()) {
			$registries =  Registry::getsBy('user_id', User::curr()->id);
		} else if(User::isDok()){
			$dok = Doktor::getBy('user_id', User::curr()->id);
			if (isset($dok)) {
				$registries = Registry::getsBy('doktor_id', $dok->id);
			}
		} else {
			$registries = Registry::all();
		}
		return view('account.registries')->with([
			'cur_role' => $this->cur_role,
			'registries' => $registries,
		]);
	}
	public function Users() {
		$this->the_before();
		return view('account.users')->with([
			'cur_role' => $this->cur_role,
			'users' => User::all(),
		]);
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
