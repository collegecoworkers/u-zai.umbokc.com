<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	User
};

class SiteController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {
		$this->the_before();
		return view('index')->with([
			'cur_role' => $this->cur_role,
		]);
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
