<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	User,
	Doktor,
	Entity,
	Disease,
	Registry,
	Specification
};

class DiseaseController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function View($id) {
		$this->the_before();
		$user = User::getBy('id', $id);
		$diseases = Disease::getsBy('user_id', $user->id);
		return view('disease.view')->with([
			'cur_role' => $this->cur_role,
			'user' => $user,
			'diseases' => $diseases,
		]);
	}
	public function Add($id) {
		$this->the_before();
		return view('disease.add')->with([
			'cur_role' => $this->cur_role,
			'user' => User::getBy('id', $id),
		]);
	}
	public function Edit($id) {
		$this->the_before();
		$model = Disease::getBy('id', $id);
		return view('disease.edit')->with([
			'cur_role' => $this->cur_role,
			'model' => $model,
		]);
	}
	public function Delete($id) {
		$this->the_before();
		Disease::where('id', $id)->delete();
		return redirect()->to('/account/patients');
	}
	public function Create($id, Request $request) {
		$this->the_before();
		$model = new Disease();

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->user_id = $id;
		$model->doktor_id = auth()->user()->id;

		$model->save();
		return redirect('/account/patients');
	}
	public function Update($id, Request $request) {
		$this->the_before();
		$model = Disease::getBy('id', $id);

		$model->title = request()->title;
		$model->desc = request()->desc;

		$model->save();
		return redirect()->to('/account/patients');
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
