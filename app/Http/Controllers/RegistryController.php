<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	User,
	Doktor,
	Entity,
	Registry,
	Specification
};

class RegistryController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function StepEntity() {
		$this->the_before();
		return view('registry.entity')->with([
			'cur_role' => $this->cur_role,
			'entities' => Entity::allArr(),
		]);
	}
	public function StepSpecific($id) {
		$this->the_before();
		$doktors = Doktor::getsBy('entity_id', $id);
		$specific_ids = [];
		foreach ($doktors as $item) {
			if(!in_array($item->specific_id, $specific_ids)){
				$specific_ids[] = $item->specific_id;
			}
		}
		$specifications = Specification::whereIn('id', $specific_ids)->get();
		$specifications = F::toArr($specifications, 'title');

		return view('registry.specific')->with([
			'cur_role' => $this->cur_role,
			'specifications' => $specifications,
		]);
	}
	public function Index($e_id, $s_id) {
		$this->the_before();
		$doktors = F::toArr(Doktor::where(['entity_id' => $e_id, 'specific_id' => $s_id,])->get(), function($item){
			return User::getBy('id', $item->user_id)->full_name;
		});
		return view('registry.index')->with([
			'cur_role' => $this->cur_role,
			'doktors' => $doktors,
		]);
	}
	public function Create(Request $request) {
		$model = new Registry();
		$d = request()->date;
		$date = date('Y-m-d H:i:s', strtotime("{$d['year']}-{$d['month']}-{$d['day']} {$d['hour']}:{$d['minute']}:00"));

		$model->title = request()->title;
		$model->date = $date;
		$model->doktor_id = request()->doktor_id;
		$model->user_id = auth()->user()->id;
		$model->desc = request()->desc;
		$model->status = '0';

		$model->save();
		return redirect('/account/registries');
	}

	public function Edit($id) {
		$this->the_before();
		$model = Registry::where('id', $id)->first();
		$doktors = F::toArr(Doktor::all(), function($item){
			return User::getBy('id', $item->user_id)->full_name;
		});
		return view('registry.edit')->with([
			'cur_role' => $this->cur_role,
			'model' => $model,
			'doktors' => $doktors,
			'statuses' => Registry::getStatuss(),
		]);
	}
	public function Delete($id) {
		Registry::where('id', $id)->delete();
		return redirect()->to('/');
	}
	public function Update($id, Request $request) {
		$model = Registry::where('id', $id)->first();

		$d = request()->date;
		$date = date('Y-m-d H:i:s', strtotime("{$d['year']}-{$d['month']}-{$d['day']} {$d['hour']}:{$d['minute']}:00"));

		$model->title = request()->title;
		$model->date = $date;
		$model->doktor_id = request()->doktor_id;
		$model->status = request()->status;
		$model->desc = request()->desc;

		$model->save();
		return redirect('/account/registries');
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
