<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@Index')->name('/');

Route::get('/account', 'AccountController@Index')->name('/account');
Route::get('/account/history', 'AccountController@History')->name('/account/history');
Route::get('/account/patients', 'AccountController@Patients')->name('/account/patients');
Route::get('/account/entities', 'AccountController@Entities')->name('/account/entities');
Route::get('/account/specifications', 'AccountController@Specifications')->name('/account/specifications');
Route::get('/account/doktors', 'AccountController@Doktors')->name('/account/doktors');
Route::get('/account/registries', 'AccountController@Registries')->name('/account/registries');
Route::get('/account/users', 'AccountController@Users')->name('/account/users');

Route::get('/disease/view/{id}', 'DiseaseController@View')->name('/disease/view/{id}');
Route::get('/disease/add/{id}', 'DiseaseController@Add')->name('/disease/add/{id}');
Route::get('/disease/edit/{id}', 'DiseaseController@Edit')->name('/disease/edit/{id}');
Route::get('/disease/delete/{id}', 'DiseaseController@Delete')->name('/disease/delete/{id}');
Route::post('/disease/create/{id}', 'DiseaseController@Create')->name('/disease/create/{id}');
Route::post('/disease/update/{id}', 'DiseaseController@Update')->name('/disease/update/{id}');

// registry
	Route::get('/registry', 'RegistryController@StepEntity')->name('/registry');
	Route::get('/registry/ss/{id}', 'RegistryController@StepSpecific')->name('/registry/{id}');
	Route::get('/registry/ss/{e_id}/{s_id}', 'RegistryController@Index')->name('/registry/{e_id}/{s_id}');
	Route::get('/registry/edit/{id}', 'RegistryController@Edit')->name('/registry/edit/{id}');
	Route::get('/registry/delete/{id}', 'RegistryController@Delete')->name('/registry/delete/{id}');
	Route::post('/registry/create', 'RegistryController@Create')->name('/registry/create');
	Route::post('/registry/update/{id}', 'RegistryController@Update')->name('/registry/update/{id}');
// end registry

// entity
	Route::get('/entity/add', 'EntityController@Add')->name('/entity/add');
	Route::get('/entity/edit/{id}', 'EntityController@Edit')->name('/entity/edit/{id}');
	Route::get('/entity/delete/{id}', 'EntityController@Delete')->name('/entity/delete/{id}');
	Route::post('/entity/create', 'EntityController@Create')->name('/entity/create');
	Route::post('/entity/update/{id}', 'EntityController@Update')->name('/entity/update/{id}');
// end entity

// specification
	Route::get('/specification/add', 'SpecificationController@Add')->name('/specification/add');
	Route::get('/specification/edit/{id}', 'SpecificationController@Edit')->name('/specification/edit/{id}');
	Route::get('/specification/delete/{id}', 'SpecificationController@Delete')->name('/specification/delete/{id}');
	Route::post('/specification/create', 'SpecificationController@Create')->name('/specification/create');
	Route::post('/specification/update/{id}', 'SpecificationController@Update')->name('/specification/update/{id}');
// end specification

// doktor
	Route::get('/doktor/add', 'DoktorController@Add')->name('/doktor/add');
	Route::get('/doktor/edit/{id}', 'DoktorController@Edit')->name('/doktor/edit/{id}');
	Route::get('/doktor/delete/{id}', 'DoktorController@Delete')->name('/doktor/delete/{id}');
	Route::post('/doktor/create', 'DoktorController@Create')->name('/doktor/create');
	Route::post('/doktor/update/{id}', 'DoktorController@Update')->name('/doktor/update/{id}');
// end doktor

// user
	Route::get('/user/add', 'UserController@Add')->name('/user/add');
	Route::get('/user/edit/{id}', 'UserController@Edit')->name('/user/edit/{id}');
	Route::get('/user/delete/{id}', 'UserController@Delete')->name('/user/delete/{id}');
	Route::post('/user/create', 'UserController@Create')->name('/user/create');
	Route::post('/user/update/{id}', 'UserController@Update')->name('/user/update/{id}');
// edn user
